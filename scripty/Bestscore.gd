extends Node

func _ready():
	# LOAD NORMAL / HC HI SCORE
	high_score.load_bestscore()
	get_node("Score").set_text(String(high_score.bestscore["normalscore"]))
	get_node("Score2").set_text(String(high_score.bestscore["hardscore"]))

# MENU BUTTON
func _on_menu_pressed():
	get_tree().change_scene("res://sceny/menu.tscn")

# RESET BUTTON
func _on_reset_score_pressed():
	
	# RESET VALUES
	high_score.set_bestscore("normalscore", 0)
	high_score.set_bestscore("hardscore", 0)
	
	# SET TEXT TO 0
	get_node("Score").set_text(String(high_score.bestscore["normalscore"]))
	get_node("Score2").set_text(String(high_score.bestscore["hardscore"]))
