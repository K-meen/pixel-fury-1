extends Node

# SINGLETON CHECK
var hardcore_enabled = false
# HARDCORE FILE
var hardcore_file = File.new()
const filepath_hardcore = "user://hardcore.data"

func _ready():
	 pass
	
# LOAD / SAVE / SET FUNKCIE
func load_hardcore():
	if not hardcore_file.file_exists(filepath_hardcore): return
	hardcore_file.open(filepath_hardcore, File.READ)
	hardcore_enabled = hardcore_file.get_var()
	hardcore_file.close()
	
func save_hardcore():
	hardcore_file.open(filepath_hardcore, File.WRITE)
	hardcore_file.store_var(hardcore_enabled)
	hardcore_file.close()

func set_hardcore(new_value):
	hardcore_enabled = new_value
	save_hardcore()
