extends Node

# SINGLETON CHECK
var currenths = 0
var bestscore = {
	"normalscore" : 0,
	"hardscore" : 0
}

# SCORE FILE
var file = File.new()
const filepath = "user://bestscore.data"

# LOAD / SAVE / SET FUNKCIE
func load_bestscore():
	if not file.file_exists(filepath): return
	file.open(filepath, File.READ)
	bestscore = file.get_var()
	file.close()

func save_bestscore():
	file.open(filepath, File.WRITE)
	file.store_var(bestscore)
	file.close()

func set_bestscore(key, new_value):
	bestscore[key] = new_value
	save_bestscore()