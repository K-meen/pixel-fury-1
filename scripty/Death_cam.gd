extends Node

var normalscore = high_score.bestscore["normalscore"]
var hardscore = high_score.bestscore["hardscore"]

func _ready():
	
	# PLAY DEFEAT THEME
	audio.get_node("menu_theme")._set_playing(true)
	
	# HC MODE ENABLED
	if hardcore.hardcore_enabled:
		get_node("Score").add_color_override("font_color", Color(255,0,0,255))
		
		# IF NEW HS
		if(high_score.currenths > hardscore):
			high_score.set_bestscore("hardscore", high_score.currenths)
			get_node("HS").set_text("new hardmode high score")
			get_node("HS").add_color_override("font_color", Color(255,0,0,255))
			$HS.visible = true
			
		else:
			$HS.visible = false
			
	# HC MODE DISABLED
	else:
		# IF NEW HS
		if(high_score.currenths > normalscore):
			high_score.set_bestscore("normalscore", high_score.currenths)
			get_node("HS").set_text("new high score")
			$HS.visible = true
			
		else:
			$HS.visible = false
	
	# SHOW HS AND COIN AMOUNT
	get_node("coins_amount").set_text(String(high_score.currenths / 5))
	get_node("Score").set_text(String(high_score.currenths))	
	
	# SAVE COIN, ABILITY
	pocet_penazi.pocet_penazi += high_score.currenths / 5
	pocet_penazi.save_money()
	pocet_penazi.save_ability()
		
		
# BUTTONS
func _on_TextureButton_pressed():
	get_tree().change_scene("res://sceny/spawner_nepriatel.tscn")

func _on_TextureButton2_pressed():
	get_tree().change_scene("res://sceny/menu.tscn")
