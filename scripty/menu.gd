extends Node

var zmen_text_hard = load("res://buttons/death_button_pressed.png")
var zmen_text_normal = load("res://buttons/death_button.png")

func _ready():
	
	# LOAD HC 
	hardcore.load_hardcore()
	
	#HC ON
	if hardcore.hardcore_enabled == true:
		$Hardcore.set_normal_texture(zmen_text_hard)
		
# PLAY
func _on_TextureButton_pressed():
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://sceny/spawner_nepriatel.tscn")

# SCORE
func _on_TextureButton2_pressed():
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://sceny/Bestscore.tscn")

# SHOP
func _on_shop_pressed():
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://sceny/Shop.tscn")

# HC MODE BUTTON
func _on_Hardcore_pressed():
	if hardcore.hardcore_enabled == false:
		hardcore.set_hardcore(true)
		print("harcore on !")
		$Hardcore.set_normal_texture(zmen_text_hard)
		
	else:
		hardcore.set_hardcore(false)
		print("harcore off !")
		$Hardcore.set_normal_texture(zmen_text_normal)
	
