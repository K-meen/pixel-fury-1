extends Node

const pravygob = preload("res://sceny/goblin_right.tscn")
const lavygob = preload("res://sceny/goblin_left.tscn")
const mainchr = preload("res://sceny/Player.tscn")

# warning-ignore:unused_class_variable
var hero = mainchr.instance()
var timer = null
var enemyP = null
var enemyL = null
var strana = null
var i = 1.5
# warning-ignore:unused_class_variable
var play_main = load("res://sounds/menu_theme.ogg")


# warning-ignore:unused_argument
func _process(delta):
	
	if Input.is_action_just_pressed("ui_right"):
		zrus_shake()
	
	
	if Input.is_action_just_pressed("ui_left"):
		zrus_shake()


func _ready():
	
	# TURN OFF MENU MUSIC
	audio.get_node("menu_theme")._set_playing(false)
	
	# CONNECT SIGNALS "stopshaking_now"

	get_node("Player").connect("zmena_score",self, "zratavanie_score")

	get_node("Player").connect("shakeit",self, "start_shaking")

	get_node("Player").connect("stopshaking",self, "zrus_shake")

	get_node("Player").connect("reg_attack",self, "zahraj_shake_utok")

	get_node("Player").connect("sound",self, "zahraj_utok_audio")
	
	# START ANIMATION TIMER
	timer = Timer.new()
	timer.set_one_shot(true)
	timer.set_wait_time(1.5)
	timer.connect("timeout", self, "on_timeout_complete")
	add_child(timer)
	pockaj_na_anim()
	timer.start()
	
	
	# CHECK FOR ABILITY
	pocet_penazi.load_ability()
	if(pocet_penazi.ffield_active == true):
		$AbilityButton.visible = true
		
	# HC MODE 
	if hardcore.hardcore_enabled:
		_set_harcore_areas(true)
		
	# NORMAL MODE
	else:
		_set_harcore_areas(false)
	
	# RESET SCORE
	high_score.currenths = 0
	
	
# PLAY START ANIMATION
func pockaj_na_anim():
	timer.set_wait_time(i)
	get_node("Player/mainchr/mainchar/characterplayer").play("uvod_char")	
	
	
# SET HC AREA MONITORING
func _set_harcore_areas(state):
	$Player/Node/RightSlower.monitoring = state
	$Player/Node/LeftSlower.monitoring = state	
	
	
# SCORE
func zratavanie_score():
	high_score.currenths += 1
	get_node("Camera2D/Label").set_text(String(high_score.currenths))
	
	
# SPAWN TIMER
func on_timeout_complete():
	spawn()
	timer.start()

	
# SET SPAWN TIME
func adjust_spawntime():
	randomize()
	i = randf() + 0.5
	
	timer.set_wait_time(i)
	
	
# SET GOBLIN SPEED
func _set_leftgob_speed(speed):
		enemyL.get_node("lavygob").speed = speed
		
func _set_rightgob_speed(speed):
		enemyP.get_node("pravygob").speed = speed
		
		
# SPAWN GOBLIN
func spawn():
	
	enemyP = pravygob.instance()
	enemyL = lavygob.instance()
	randomize()
	
	strana = randi()%6
	# SPAWN ONE LEFT
	if(strana == 0 || strana == 2):
		if strana == 0:
			_set_leftgob_speed(300)
			
		else:
			_set_leftgob_speed(600)
			
		get_node("container").add_child(enemyL)
		adjust_spawntime()

	# SPAWN ONE RIGHT
	elif(strana == 1 || strana == 3):
		if strana == 1:
			_set_rightgob_speed(-300)
			
		else:
			_set_rightgob_speed(-600)
			
		get_node("container").add_child(enemyP)
		adjust_spawntime()
		
	# SPAWN BOTH SIDES
	else: 
		if strana == 4:
			_set_leftgob_speed(300)
			_set_rightgob_speed(-300)
			
		else:
			_set_leftgob_speed(600)
			_set_rightgob_speed(-600)
			
		get_node("container").add_child(enemyL)
		get_node("container").add_child(enemyP)
		adjust_spawntime()

# ABILITY BUTTON
func _on_AbilityButton_pressed():
	pocet_penazi.ffield_active = false
	$AbilityButton.visible = false

# ABILITY SHAKE EFFECT
func start_shaking():
	$transparent_black.visible = true
	$shaking.play("shaking_force_attack")
	$transparent_black.visible = true

# STOP SCREENSHAKE
func zrus_shake():
	$shaking.stop(true)
	$transparent_black.visible = false

#STOP SHAKING ON ANIMATION FINISH
func _on_shaking_animation_finished(anim_name):
	if(anim_name == "forceshield"):
		$shaking.stop("shaking_force_attack")
		$transparent_black.visible = false

# ATTACK SHAKE
func zahraj_shake_utok():
	$shaking.play("regular_attack_shake")

# ATTACK SOUND
func zahraj_utok_audio():
	get_node("attack_sound").playing = true

# PAUSE BUTTOn
func _on_PauseButton_pressed():
	if get_tree().paused == false:
		get_tree().paused = true
		
	else:
		get_tree().paused = false

