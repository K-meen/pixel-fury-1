extends KinematicBody2D

var velocity = Vector2()
var collision_info = null
var speed = -600

# BASIC POHYB
func _physics_process(delta):
	velocity.x = speed
	collision_info = move_and_collide(velocity * delta)

# SET SPEED
func _set_speed(i):
		speed = i
		
# SET Z INDEX
func set_z(z):
	z_index = z
	
# PO ATTACK/DEFEAT ANIMACII
func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "RIPgoblin":
		queue_free()
	if anim_name == "utokgob":
		get_tree().change_scene("res://sceny/Death_cam.tscn")