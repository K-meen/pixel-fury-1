extends Node

var Direction = "right"

# BASIC PREMENNE
var cervenygobL = preload("res://sprites/skret_cerveny.png")
var stlacanie = null
var delay_stlacenia = 1
var stlacenie = true
var score = 0
var utok = null


signal zmena_score
signal shakeit
signal stopshaking
signal reg_attack
signal sound



# SetUtility-----------------------------------------------------------------------

#KILLZONE MONITOR SET
func _set_left_killzone_state(state):
	$Node/LeftArea.monitoring = state

func _set_right_killzone_state(state):
	$Node/RightArea.monitoring = state

func _set_rl_killzone_state(state):
	$Node/LeftArea.monitoring = state
	$Node/RightArea.monitoring = state

func _character_aniplay(anim_name):
	$mainchr/mainchar/characterplayer.play(anim_name)
	
# Start -----------------------------------------------------------------------

func _ready():

# ATTACK COOLDOWN TIMER -----------------------------------------------------------------------
	
	stlacanie = Timer.new()
	stlacanie.set_wait_time(delay_stlacenia)
	stlacanie.connect("timeout", self, "hotovo_na_timeout")
	add_child(stlacanie)
	
	Engine.set_time_scale(0.5)
	
	 
# ATTACK COOLDOWN TIMER TIMEOUT------------------------------------------------

func hotovo_na_timeout():
	stlacenie = true
	
# KILLZONES -----------------------------------------------------------------------

	# KED GOBLIN VOJDE DO AREAS ****************************************************
	
func _on_Area2D2_body_entered(body):
	body._set_speed(0)
	body.get_node("Sprite/AnimationPlayer").play("RIPgoblin")
	emit_signal("zmena_score")
	if(get_node("mainchr/mainchar/characterplayer").current_animation != "forceshield"):
		emit_signal("reg_attack")
	stlacenie = true

func _on_Area3D2_body_entered(body):
	body._set_speed(0)
	body.get_node("Sprite/AnimationPlayer").play("RIPgoblin")
	emit_signal("zmena_score")
	if(get_node("mainchr/mainchar/characterplayer").current_animation != "forceshield"):
		emit_signal("reg_attack")
	stlacenie = true
	
	# KED CHARACTER ZAUTOCI ****************************************************
	
func _on_characterplayer_animation_started(anim_name):
	if anim_name == "utok1" || anim_name == "utok2":
		if Direction == "right":	
			_set_right_killzone_state(true)
		else:
			_set_left_killzone_state(true)
			
	if anim_name == "forceshield":
		_set_rl_killzone_state(true)
		emit_signal("shakeit")
		
	# KED CHAR SKONCI UTOK ****************************************************
		
func _on_characterplayer_animation_changed(old_name, new_name):
	if old_name == "utok1" || old_name == "utok2" || old_name == "forceshield":
		_set_rl_killzone_state(false)
		emit_signal("stopshaking")

# DEATHZONE -----------------------------------------------------------------------

func _on_mainchr_body_entered(body):
	if body.get_name() == "lavygob":
		if $Node/LeftArea.monitoring == false:
			body._set_speed(0)
			body.set_z(2)
			body.get_node("Sprite/AnimationPlayer").play("utokgob")
			
	else:
		if $Node/RightArea.monitoring == false:
			body._set_speed(0)
			body.set_z(2)
			body.get_node("Sprite/AnimationPlayer").play("utokgob")
			
	
# FLIP CHAR -----------------------------------------------------------------------

func _flipCharRight():
	if Direction != "right":
		$mainchr/mainchar.flip_h = false
		Direction = "right"

func _flipCharLeft():
	if Direction != "left":
		$mainchr/mainchar.flip_h = true
		Direction = "left"
		
# ON RIGHT/LEFT INPUT -----------------------------------------------------------------------

func _on_Right():
	_flipCharRight()
	
	if stlacenie == true:
		utok = randi()%2
		if(utok == 0):
			_character_aniplay("utok1")
		else:
			_character_aniplay("utok2")
			
		emit_signal("sound")
			
		# BLOCK ATTACKS AND START CD
		stlacenie = false
		stlacanie.start()

func _on_Left():
	_flipCharLeft()
	
	if stlacenie == true:
		utok = randi()%2
		if(utok == 0):
			_character_aniplay("utok1")
		else:
			_character_aniplay("utok2")
			
		emit_signal("sound")
		
		# BLOCK ATTACKS AND START CD
		stlacenie = false
		stlacanie.start()

# PROCESS -----------------------------------------------------------------------

func _process(delta):
	
	# KEYBAORD INPUT
	if Input.is_action_just_pressed("ui_right"):
		_on_Right()
	
	
	if Input.is_action_just_pressed("ui_left"):
		_on_Left()
	

# TOUCH INPUT -----------------------------------------------------------------------

func _on_touchRight_pressed():
	_on_Right()

		
func _on_touchLeft_pressed():
	_on_Left()

# ABILITY BUTTON
func _on_AbilityButton_pressed():
	_character_aniplay("forceshield")
	$AudioStreamPlayer.playing = true


# HC MODE BAIT AREAS
func _on_RightSlower_body_exited(body):
	randomize()
	
	if randi()%5 == 4:
		body.speed = 200
		body.get_node("Sprite").flip_h = false

func _on_LeftSlower_body_exited(body):
	randomize()
	
	if randi()%5 == 4:
		body.speed = -200
		body.get_node("Sprite").flip_h = true

