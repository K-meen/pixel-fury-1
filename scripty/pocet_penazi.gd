extends Node

# SINGLETON CHECK
var ffield_active = false
var pocet_penazi = 0

# COIN FILE
var money = File.new()
const filepath = "user://money.data"

# ABILITY FILE
var ability = File.new()
const filepath_ability = "user://forcefield_availability.data"

func _ready():
	load_money()

# COIN FUNKCIE SAVE / LOAD / SET
func load_money():
	if not money.file_exists(filepath): return
	money.open(filepath, File.READ)
	pocet_penazi = money.get_var()
	money.close()

func save_money():
	money.open(filepath, File.WRITE)
	money.store_var(pocet_penazi)
	money.close()

func set_money(new_value):
	pocet_penazi = new_value
	save_money()
	
# ABILITY FUNKCIE SAVE / LOAD / SET
func load_ability():
	if not ability.file_exists(filepath_ability): return
	ability.open(filepath_ability, File.READ)
	ffield_active = ability.get_var()
	ability.close()
	
func save_ability():
	ability.open(filepath_ability, File.WRITE)
	ability.store_var(ffield_active)
	ability.close()
	
func set_ability(ability_state):
	ffield_active = ability_state
	save_ability()
