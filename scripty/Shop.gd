extends Node

func _ready():
	
	# ENABLE / DISABLE BUY BUTTON
	if(pocet_penazi.pocet_penazi > 999):
		pocet_penazi.pocet_penazi = 999
	if(pocet_penazi.pocet_penazi < 20):
		$buy_button.disabled = true
		
	elif(pocet_penazi.ffield_active == true):
		$buy_button.disabled = true
			
	else:
		$buy_button.disabled = false
		
	# SHOW YOUR COINS
	$your_coins.set_text("You have "+String(pocet_penazi.pocet_penazi)+" coins")


# ON BUY BUTTON
func _on_TextureButton_pressed():
	pocet_penazi.pocet_penazi = pocet_penazi.pocet_penazi - 20
	$your_coins.set_text("You have "+String(pocet_penazi.pocet_penazi)+" coins")
	$buy_button.disabled = true
	pocet_penazi.set_ability(true)
	
	
# MENU
func _on_menu_pressed():
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://sceny/menu.tscn")
